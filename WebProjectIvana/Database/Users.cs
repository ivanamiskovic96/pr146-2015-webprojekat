﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using WebProjectIvana.Controllers;
using WebProjectIvana.Models;

namespace WebProjectIvana.Database
{
    public class Users
    {
        public List<Korisnik> lista_korisnika { get; set; }
        public static List<Korisnik> filter_lista { get; set; }

        public Users()
        {
            JsonHelper json = new JsonHelper();
            lista_korisnika = JsonConvert.DeserializeObject<List<Korisnik>>(json.Read(Path.Combine(HomeController.rootLocation, "Users.json")));
            if (lista_korisnika == null)
            {
                lista_korisnika = new List<Korisnik>();
            }
        }
        public bool IzmeniKorisnika(Korisnik k)
        {
            foreach (var item in lista_korisnika)
            {
                if (item.Id != k.Id && item.LogickiIzbrisan== false &&  item.KorisnickoIme == k.KorisnickoIme)
                {
                    return false;
                }
            }
            foreach (var item in lista_korisnika)
            {
                if (item.Id == k.Id)
                {
                    lista_korisnika.Remove(item);
                    break;
                }
            }
            lista_korisnika.Add(k);
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Users.json"), lista_korisnika);
            return true;
        }


        public bool DodajKorisnike(Korisnik k)
        {
            foreach (var item in lista_korisnika)
            {
                if (item.LogickiIzbrisan == false && item.KorisnickoIme == k.KorisnickoIme)
                {
                    return false;
                }
            }
            lista_korisnika.Add(k);
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Users.json"), lista_korisnika);
            return true;
        }
        public void ObrisiKorisnika(string korisnickoIme)
        {
            foreach (var item in lista_korisnika)
            {
                if (item.LogickiIzbrisan== false && item.KorisnickoIme == korisnickoIme)
                {
                    item.LogickiIzbrisan = true;
                }
            }
            foreach (var item in filter_lista)
            {
                if (item.LogickiIzbrisan == false && item.KorisnickoIme == korisnickoIme)
                {
                    filter_lista.Remove(item);
                    break;
                }
            }
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Users.json"), lista_korisnika);
        }
        public Korisnik ProveriLog(Korisnik k)
        {
            foreach (var item in VratiValidne())
            {
                if (item.LogickiIzbrisan == false && item.KorisnickoIme == k.KorisnickoIme && item.Lozinka == k.Lozinka)
                {
                    return item;
                }
            }
            return null;
        }
        public List<Korisnik> VratiValidne()
        {
            List<Korisnik> korisnici = new List<Korisnik>();
            foreach (var item in lista_korisnika)
            {
                if (item.LogickiIzbrisan == false)
                {
                    korisnici.Add(item);
                }
            }
            return korisnici;
        }
        public void Update()
        {
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Users.json"), lista_korisnika);
        }



    }
}