﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebProjectIvana.Controllers;
using WebProjectIvana.Models;

namespace WebProjectIvana.Database
{
    public class Komentari
    {
        public List<Komentar> lista_komentara { get; set; }
        public static List<Komentar> filter_lista { get; set; }

        public Komentari()
        {
            JsonHelper json = new JsonHelper();
            lista_komentara = JsonConvert.DeserializeObject<List<Komentar>>(json.Read(Path.Combine(HomeController.rootLocation, "Komentari.json")));
            if (lista_komentara == null)
            {
                lista_komentara = new List<Komentar>();
            }
        }
        public bool IzmeniKomentar(Komentar k)
        {


            lista_komentara.Add(k);
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Komentari.json"), lista_komentara);
            return true;
        }


        public bool DodajKomentar(Komentar k)
        {

            lista_komentara.Add(k);
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Komentari.json"), lista_komentara);
            return true;
        }
        public void AktivirajKomentar(int id)
        {
            foreach (var item in lista_komentara)
            {
                if (item.Id == id)
                {
                    item.IsActivated = true;
                }
            }
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Komentari.json"), lista_komentara);
        }

        public void DeaktivirajKomentar(int id)
        {
            foreach (var item in lista_komentara)
            {
                if (item.Id == id)
                {
                    item.IsActivated = false;
                }
            }
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Komentari.json"), lista_komentara);
        }
        public List<Komentar> VratiAktivne(int id)
        {
            List<Komentar> temp = new List<Komentar>();
            foreach (var item in lista_komentara)
            {
                if (item.IdManifestacije == id && item.IsActivated == true)
                {
                    temp.Add(item);
                }
            }
            return temp;
        }
        public List<Komentar> VratiNeaktivne(int id)
        {
            List<Komentar> temp = new List<Komentar>();
            foreach (var item in lista_komentara)
            {
                if (item.IdManifestacije == id && item.IsActivated == false)
                {
                    temp.Add(item);
                }
            }
            return temp;
        }

        public void Update()
        {
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Komentari.json"), lista_komentara);
        }
    }
}