﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebProjectIvana.Controllers;
using WebProjectIvana.Models;

namespace WebProjectIvana.Database
{
    public class Karte
    {
        public List<Karta> lista_karata { get; set; }
        public static List<Karta> filter_lista { get; set; }

        public Karte()
        {
            JsonHelper json = new JsonHelper();
            lista_karata = JsonConvert.DeserializeObject<List<Karta>>(json.Read(Path.Combine(HomeController.rootLocation, "Karte.json")));
            if (lista_karata == null)
            {
                lista_karata = new List<Karta>();
            }
        }
    
        public void DodajKarte(Karta k)
        {
            lista_karata.Add(k);
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Karte.json"), lista_karata);
        }
        public void ObrisiKartu(int id)
        {
            foreach (var item in lista_karata)
            {
                if (item.Id == id)
                {
                    item.LogickiIzbrisan = true;
                }
            }
            foreach (var item in filter_lista)
            {
                if (item.Id == id)
                {
                    filter_lista.Remove(item);
                    break;
                }
            }

            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Karte.json"), lista_karata);
        }

        public List<Karta> VratiAktivne(int id)
        {
            List<Karta> temp = new List<Karta>();
            foreach (var item in lista_karata)
            {
                if (item.Kupac.Id == id && item.LogickiIzbrisan == false)
                {
                    temp.Add(item);
                }
            }
            filter_lista = temp;
            return temp;
        }
        public List<Karta> VratiSve()
        {
            List<Karta> temp = new List<Karta>();
            foreach (var item in lista_karata)
            {
                if (item.LogickiIzbrisan == false)
                {
                    temp.Add(item);
                }
            }
            filter_lista = temp;
            return temp;
        }

        public void Update()
        {
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Karte.json"), lista_karata);
        }
    }
}