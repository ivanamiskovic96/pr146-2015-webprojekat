﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebProjectIvana.Controllers;
using WebProjectIvana.Models;

namespace WebProjectIvana.Database
{
    public class Manifestacije
    {
        public List<Manifestacija> lista_manifestacija { get; set; }
        public static List<Manifestacija> filter_lista { get; set; }

        public Manifestacije()
        {
            JsonHelper json = new JsonHelper();
            lista_manifestacija = JsonConvert.DeserializeObject<List<Manifestacija>>(json.Read(Path.Combine(HomeController.rootLocation, "Manifestacije.json")));
            if (lista_manifestacija == null)
            {
                lista_manifestacija = new List<Manifestacija>();
            }

            for (int i = 0; i < lista_manifestacija.Count - 1; i++)
            {
                for (int j = i+1; j < lista_manifestacija.Count; j++)
                {
                    if (lista_manifestacija[i].Datum > lista_manifestacija[j].Datum)
                    {
                        Manifestacija temp = lista_manifestacija[i];
                        lista_manifestacija[i] = lista_manifestacija[j];
                        lista_manifestacija[j] = temp;
                    }
                }
            }

        }
        public bool DodajManifestaciju(Manifestacija m)
        {
            foreach (var item in lista_manifestacija)
            {
                if (item.LogickiIzbrisan == false && item.Lokacija.IsEqual(m.Lokacija))
                {
                    return false;
                }
            }
            lista_manifestacija.Add(m);
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Manifestacije.json"), lista_manifestacija);
            return true;
        }
        public bool IzmeniManifestaciju(Manifestacija m)
        {
            foreach (var item in lista_manifestacija)
            {
                if (item.Id != m.Id && item.Lokacija.IsEqual(m.Lokacija))
                {
                    return false;
                }
            }
            foreach (var item in lista_manifestacija)
            {
                if (item.Id == m.Id)
                {
                    lista_manifestacija.Remove(item);
                    lista_manifestacija.Add(m);
                    break;
                }
            }
            foreach (var item in filter_lista)
            {
                if (item.Id == m.Id)
                {
                    filter_lista.Remove(item);
                    filter_lista.Add(m);
                    break;
                }
            }
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Manifestacije.json"), lista_manifestacija);
            return true;
        }

        public void ObrisiManifestaciju(int id)
        {
            foreach (var item in lista_manifestacija)
            {
                if (item.Id == id)
                {
                    item.LogickiIzbrisan = true;
                }
            }
            foreach (var item in filter_lista)
            {
                if (item.Id == id)
                {
                    filter_lista.Remove(item);
                    break;
                }
            }
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Manifestacije.json"), lista_manifestacija);
        }
        public List<Manifestacija> VratiValidne()
        {
            List<Manifestacija> manifestacije = new List<Manifestacija>();
            foreach (var item in lista_manifestacija)
            {
                if (item.LogickiIzbrisan == false)
                {
                    manifestacije.Add(item);
                }
            }
            filter_lista = manifestacije;

            return manifestacije;
        }
        public Manifestacija Pronadji(int id)
        {
            foreach (var item in lista_manifestacija)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }
            return null;
        }

        public List<Manifestacija> VratiAktivne()
        {
            List<Manifestacija> manifestacije = new List<Manifestacija>();
            foreach (var item in lista_manifestacija)
            {
                if (item.LogickiIzbrisan == false && item.Status == Status.Aktivan)
                {
                    manifestacije.Add(item);
                }
            }
            filter_lista = manifestacije;
            return manifestacije;
        }
        public Manifestacija VratiManifestaicju(int id)
        {
            List<Manifestacija> manifestacije = new List<Manifestacija>();
            foreach (var item in lista_manifestacija)
            {
                if (item.Id==id)
                {
                    return item;
                }
            }
            return null;
        }

        public List<Manifestacija> VratiNeaktivne()
        {
            List<Manifestacija> manifestacije = new List<Manifestacija>();
            foreach (var item in lista_manifestacija)
            {
                if (item.LogickiIzbrisan == false && item.Status == Status.Neaktivan)
                {
                    manifestacije.Add(item);
                }
            }
            filter_lista = manifestacije;

            return manifestacije;
        }
        public List<Manifestacija> VratiValidneSaId(int id)
        {
            List<Manifestacija> manifestacije = new List<Manifestacija>();
            foreach (var item in lista_manifestacija)
            {
                if (item.LogickiIzbrisan == false && item.IdKorisnika == id)
                {
                    manifestacije.Add(item);
                }
            }
            filter_lista = manifestacije;

            return manifestacije;
        }
        public void AktivirajManifestaciju(int id)
        {
            foreach (var item in lista_manifestacija)
            {
                if (item.Id == id)
                {
                    item.Status = Status.Aktivan;
                }
            }
            foreach (var item in filter_lista)
            {
                if (item.Id == id)
                {
                    item.Status = Status.Aktivan;
                    break;
                }
            }
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Manifestacije.json"), lista_manifestacija);
        }

        public void DeaktivirajManifestaciju(int id)
        {
            foreach (var item in lista_manifestacija)
            {
                if (item.Id == id)
                {
                    item.Status = Status.Neaktivan;
                }
            }
            foreach (var item in filter_lista)
            {
                if (item.Id == id)
                {
                    item.Status = Status.Neaktivan;
                    break;
                }
            }
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Manifestacije.json"), lista_manifestacija);
        }



        public void Update()
        {
            JsonHelper json = new JsonHelper();
            json.Write(Path.Combine(HomeController.rootLocation, "Manifestacije.json"), lista_manifestacija);
        }
    }
}