﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjectIvana.Models
{
    public class Lokacija
    {
        public double GeografskaDuzina { get; set; }
        public double GeografskaSirina { get; set; }
        public string Grad { get; set; }
        public string Ulica { get; set; }
        public string BrojUlice { get; set; }
        public int PostanskiBroj { get; set; }

        public Lokacija(double geografskaDuzina, double geografskaSirina, string grad, string ulica, string brojUlice, int postanskiBroj)
        {
            GeografskaDuzina = geografskaDuzina;
            GeografskaSirina = geografskaSirina;
            Grad = grad;
            Ulica = ulica;
            BrojUlice = brojUlice;
            PostanskiBroj = postanskiBroj;
        }
        public bool IsEqual(object obj)
        {
            Lokacija lok = (Lokacija)obj;
            return GeografskaDuzina == lok.GeografskaDuzina && GeografskaSirina == lok.GeografskaSirina && Grad == lok.Grad && Ulica == lok.Ulica && BrojUlice == lok.BrojUlice; 
        }
    }
}