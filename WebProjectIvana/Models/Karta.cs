﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjectIvana.Models
{
    public class Karta
    {
        public int Id { get; set; }
        public string NazivManifestacije { get; set; }
        public DateTime Datum { get; set; }
        public int Cena { get; set; }
        public Korisnik Kupac { get; set; }
        public TipKarte TipKarte { get; set; }
        public bool LogickiIzbrisan { get; set; }
        public Karta(int id, string nazivManifestacije, DateTime datum, int cena, Korisnik kupac, TipKarte tipKarte)
        {
            LogickiIzbrisan = false;
            Id = id;
            NazivManifestacije = nazivManifestacije;
            Datum = datum;
            Cena = cena;
            Kupac = kupac;
            TipKarte = tipKarte;
        }
    }
}