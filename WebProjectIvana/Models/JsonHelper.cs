﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebProjectIvana.Models
{
    public class JsonHelper
    {
        public string Read(string file_name)
        {
            if (File.Exists(file_name))
            {
                return System.IO.File.ReadAllText(file_name);
            }
            return "";
        }
        public void Write(string file_name, object write_data)
        {
            System.IO.File.WriteAllText(file_name, JsonConvert.SerializeObject(write_data));
        }
    }
}