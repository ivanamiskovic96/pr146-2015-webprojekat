﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjectIvana.Models
{
    public class Manifestacija
    {
        public int Id { get; set; }
        public int IdKorisnika { get; set; }
        public string Naziv { get; set; }
        public TipManifestacije TipManifestacije { get; set; }
        public int BrojMesta { get; set; }
        public DateTime Datum { get; set; }
        public double CenaKarte { get; set; }
        public Status Status { get; set; }
        public string PosterSlika { get; set; }
        public Lokacija Lokacija { get; set; }
        public bool LogickiIzbrisan { get; set; }
        public Manifestacija(int id,int idKorisnika,string naziv, TipManifestacije tipManifestacije, int brojMesta, DateTime datum, double cenaKarte, Status status, string posterSlika, Lokacija lokacija)
        {
            IdKorisnika = idKorisnika;
            LogickiIzbrisan = false;
            Id = id;
            Naziv = naziv;
            TipManifestacije = tipManifestacije;
            BrojMesta = brojMesta;
            Datum = datum;
            CenaKarte = cenaKarte;
            Status = status;
            PosterSlika = posterSlika;
            Lokacija = lokacija;
        }
    }
}