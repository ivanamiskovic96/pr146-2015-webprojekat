﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebProjectIvana.Models;

namespace WebProjectIvana.Models
{
    public class Korisnik
    {
        public int Id { get; set; }
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public Pol Pol { get; set; }
        public Uloga Uloga { get; set; }
        public string DatumRodjenja { get; set; }
        public int BrojBodova { get; set; }
        public TipKorisnika TipKorisnika { get; set; }
        public bool LogickiIzbrisan { get; set; }
        public Korisnik()
        {
            Uloga = Uloga.Kupac;
            TipKorisnika = TipKorisnika.Bronzani;
            BrojBodova = 0;
            LogickiIzbrisan = false;
        }

        public Korisnik(string korisnickoIme, string lozinka)
        {
            this.KorisnickoIme = korisnickoIme;
            this.Lozinka = lozinka;
        }

        public Korisnik(int id, string korisnickoIme, string lozinka, string ime, string prezime, Pol pol, Uloga uloga, string datumRodjenja, int brojBodova, TipKorisnika tipKorisnika)
        {
            this.Id = id;
            this.LogickiIzbrisan = false;
            this.KorisnickoIme = korisnickoIme;
            this.Lozinka = lozinka;
            this.Ime = ime;
            this.Prezime = prezime;
            this.Pol = pol;
            this.Uloga = uloga;
            this.DatumRodjenja = datumRodjenja;
            this.BrojBodova = brojBodova;
            this.TipKorisnika = tipKorisnika;
        }
    }
}