﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebProjectIvana.Models
{
    public class Komentar
    {
        public int Id { get; set; }
        public string KorisnickoIme { get; set; }
        public int IdManifestacije { get; set; }
        public string Tekst { get; set; }
        public int Ocena { get; set; }
        public bool IsActivated { get; set; }
        public Komentar(int id,string korisnickoIme, int idManifestacije, string tekst, int ocena, bool isActivated)
        {
            IsActivated = isActivated;
            Id = id;
            KorisnickoIme = korisnickoIme;
            IdManifestacije = idManifestacije;
            Tekst = tekst;
            Ocena = ocena;
        }
    }
}