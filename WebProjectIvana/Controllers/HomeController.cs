﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Windows.Forms;
using WebProjectIvana.Database;
using WebProjectIvana.Models;

namespace WebProjectIvana.Controllers
{
    public class HomeController : Controller
    {
      

        public static string rootLocation = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6, System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Length-10);
        public static Users users = new Users();
        public static Manifestacije manifestacije = new Manifestacije();
        public static Komentari komentari = new Komentari();
        public static Karte karte = new Karte();
        public ActionResult Index()
        {
         
            users = new Users();

            manifestacije = new Manifestacije();

            Korisnik ulogovani = (Korisnik)Session["Ulogovan"];
            if (ulogovani == null)
            {
                Session["Manifestacije"] = manifestacije.VratiAktivne();
                return View("Index");
            }
            else if (ulogovani.Uloga == Uloga.Kupac)
            {
                Session["Manifestacije"] = manifestacije.VratiAktivne();
                return View("KupacHomepageView");
            }
            else if (ulogovani.Uloga == Uloga.Prodavac)
            {
                Session["Manifestacije"] = manifestacije.VratiValidneSaId(ulogovani.Id);
                return View("ProdavacHomepageView");
            }
            else if (ulogovani.Uloga == Uloga.Administrator)
            {
                Session["Manifestacije"] = manifestacije.VratiValidne();
                return View("AdminHomepageView");
            }
            else
            {
                return View();
            }
        }


        public ActionResult Izloguj()
        {
            Session["Ulogovan"] = null;
            return Index();           
        }
        public ActionResult IzmeniProfil()
        {
            return View("IzmeniKorisnikaView");
        }
 
        public ActionResult SortirajKorisnike()
        {
            int tipSortiranja = Int32.Parse(Request["TipSortiranja"]);
            int rast = Int32.Parse(Request["Rast"]);

            if (tipSortiranja == 0)
            {// IME
                if (rast == 0)
                {
                    for (int i = 0; i < Users.filter_lista.Count-1; i++)
                    {
                        for (int j = i+1; j < Users.filter_lista.Count; j++)
                        {
                            if (Users.filter_lista[i].Ime.CompareTo(Users.filter_lista[j].Ime) > 0)
                            {
                                Korisnik temp = Users.filter_lista[i];
                                Users.filter_lista[i] = Users.filter_lista[j];
                                Users.filter_lista[j] = temp;
                            }
                        }
                    }
                }
                else if (rast ==1)
                {
                    for (int i = 0; i < Users.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Users.filter_lista.Count; j++)
                        {
                            if (Users.filter_lista[i].Ime.CompareTo(Users.filter_lista[j].Ime) < 0)
                            {
                                Korisnik temp = Users.filter_lista[i];
                                Users.filter_lista[i] = Users.filter_lista[j];
                                Users.filter_lista[j] = temp;
                            }
                        }
                    }
                }
            }
            else if (tipSortiranja == 1)
            {//PREZIME
                if (rast == 0)
                {
                    for (int i = 0; i < Users.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Users.filter_lista.Count; j++)
                        {
                            if (Users.filter_lista[i].Prezime.CompareTo(Users.filter_lista[j].Prezime) > 0)
                            {
                                Korisnik temp = Users.filter_lista[i];
                                Users.filter_lista[i] = Users.filter_lista[j];
                                Users.filter_lista[j] = temp;
                            }
                        }
                    }
                }
                else if (rast == 1)
                {
                    for (int i = 0; i < Users.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Users.filter_lista.Count; j++)
                        {
                            if (Users.filter_lista[i].Prezime.CompareTo(Users.filter_lista[j].Prezime) < 0)
                            {
                                Korisnik temp = Users.filter_lista[i];
                                Users.filter_lista[i] = Users.filter_lista[j];
                                Users.filter_lista[j] = temp;
                            }
                        }
                    }
                }
            }
            else if (tipSortiranja == 2)
            {//KOR IME
                if (rast == 0)
                {
                    for (int i = 0; i < Users.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Users.filter_lista.Count; j++)
                        {
                            if (Users.filter_lista[i].KorisnickoIme.CompareTo(Users.filter_lista[j].KorisnickoIme) > 0)
                            {
                                Korisnik temp = Users.filter_lista[i];
                                Users.filter_lista[i] = Users.filter_lista[j];
                                Users.filter_lista[j] = temp;
                            }
                        }
                    }
                }
                else if (rast == 1)
                {
                    for (int i = 0; i < Users.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Users.filter_lista.Count; j++)
                        {
                            if (Users.filter_lista[i].KorisnickoIme.CompareTo(Users.filter_lista[j].KorisnickoIme) < 0)
                            {
                                Korisnik temp = Users.filter_lista[i];
                                Users.filter_lista[i] = Users.filter_lista[j];
                                Users.filter_lista[j] = temp;
                            }
                        }
                    }
                }
            }
            else if (tipSortiranja == 3)
            {//BODOVI
                if (rast == 0)
                {
                    for (int i = 0; i < Users.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Users.filter_lista.Count; j++)
                        {
                            if (Users.filter_lista[i].BrojBodova > Users.filter_lista[j].BrojBodova)
                            {
                                Korisnik temp = Users.filter_lista[i];
                                Users.filter_lista[i] = Users.filter_lista[j];
                                Users.filter_lista[j] = temp;
                            }
                        }
                    }
                }
                else if (rast == 1)
                {
                    for (int i = 0; i < Users.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Users.filter_lista.Count; j++)
                        {
                            if (Users.filter_lista[i].BrojBodova < Users.filter_lista[j].BrojBodova)
                            {
                                Korisnik temp = Users.filter_lista[i];
                                Users.filter_lista[i] = Users.filter_lista[j];
                                Users.filter_lista[j] = temp;
                            }
                        }
                    }
                }
            }
            return View("AdminPrikazKorisnikaView");
        }

        public ActionResult FilterKorisnika()
        {
            users = new Users();
            string ime = Request["Ime"];
            string prezime = Request["Prezime"];
            string korisnickoIme = Request["KorisnickoIme"];
            int uloga = Int32.Parse(Request["Uloga"]);
            int tipKorisnika = Int32.Parse(Request["TipKorisnika"]);

            Users.filter_lista = users.VratiValidne();
            foreach (var item in Users.filter_lista.ToList())
            {
                if (ime != "" && item.Ime.StartsWith(ime)== false)
                {
                    Users.filter_lista.Remove(item);
                }
                if (prezime != "" && item.Prezime.StartsWith(prezime) == false)
                {
                    Users.filter_lista.Remove(item);
                }
                if (korisnickoIme != "" && item.KorisnickoIme.StartsWith(korisnickoIme) == false)
                {
                    Users.filter_lista.Remove(item);
                }
                if (uloga != -1 && item.Uloga != (Uloga)uloga)
                {
                    Users.filter_lista.Remove(item);
                }
                if (tipKorisnika != -1 && item.TipKorisnika != (TipKorisnika)tipKorisnika)
                {
                    Users.filter_lista.Remove(item);

                }
            }
            Session["Korisnici"] = Users.filter_lista;
            return View("AdminPrikazKorisnikaView");
        }
        public ActionResult ObrisiKorisnika()
        {
            users = new Users();
            users.ObrisiKorisnika(Request["KorisnickoIme"]);
            Session["Korisnici"] = Users.filter_lista;
            return View("AdminPrikazKorisnikaView");
        }
        public ActionResult PrikazKorisnika()
        {
            users = new Users();
            Users.filter_lista = users.VratiValidne();
            Session["Korisnici"] = Users.filter_lista;
            return View("AdminPrikazKorisnikaView");
        }
        public ActionResult RegistrujKorisnika()
        {
            return View("RegistrujKorisnikaView");
        }
        public ActionResult UlogujSe()
        {
            bool ima = false;
            foreach (var item in users.lista_korisnika)
            {
                if (item.Id == 0)
                {
                    item.LogickiIzbrisan = false;
                    users.Update();
                    ima = true;
                }
            }
            if (ima == false)
            {
                users.DodajKorisnike(new Korisnik(0,"admin", "admin", "admin", "admin", Pol.MUSKI, Uloga.Administrator, "11/11/11", 0, TipKorisnika.Bronzani));
            }
            return View("UlogujSeView");
        }
        [HttpPost]
        public ActionResult IzmeniPodatke()
        {
            int godina;
            int mesec;
            int dan;
            if (Int32.TryParse(Request["GodinaRodjenja"], out godina) == false || Int32.TryParse(Request["MesecRodjenja"], out mesec) == false || Int32.TryParse(Request["DanRodjenja"], out dan) == false)
            {
                TempData["Alert"] = "Pogresno uneti podaci";
                return View("IzmeniKorisnikaView");
            }
            if (Request["KorisnickoIme"] == "" || Request["Lozinka"] == "" || Request["Ime"] == "" || Request["Ime"].Trim(' ').Any(char.IsDigit) || Request["Prezime"].Trim(' ').Any(char.IsDigit) || Request["Prezime"] == "" || (mesec < 1 || mesec > 12) || (dan < 1 || dan > 31) || godina>=DateTime.Now.Year)
            {
                TempData["Alert"] = "Pogresno uneti podaci";
                return View("IzmeniKorisnikaView");
            }
            users = new Users();
            Korisnik k = new Korisnik();
            Korisnik ulogovan = (Korisnik)Session["Ulogovan"];
            k.Id = ulogovan.Id;
            k.Uloga = ulogovan.Uloga;
 
            k.KorisnickoIme = Request["KorisnickoIme"];
            k.Lozinka = Request["Lozinka"];
            k.Ime = Request["Ime"];
            k.Prezime = Request["Prezime"];
            string s = Request["Pol"];

            if (Request["Pol"] == "Muski")
            {
                k.Pol = Pol.MUSKI;
            }
            else
            {
                k.Pol = Pol.ZENSKI;
            }
            k.DatumRodjenja = Request["GodinaRodjenja"] + "/" + Request["MesecRodjenja"] + "/" + Request["DanRodjenja"];
            if (users.IzmeniKorisnika(k))
            {
                Session["Ulogovan"] = k;             
                if (k.Uloga == Uloga.Administrator)
                {
                    return View("AdminHomepageView");
                }
                else if (k.Uloga == Uloga.Prodavac)
                {
                    return View("ProdavacHomepageView");
                }
                else
                {
                    return View("KupacHomepageView");
                }
            }
            else
            {
                TempData["Alert"] = "Vec postoji korisnik sa tim korisnickim imenom";
                return View("IzmeniKorisnikaView");
            }
        }

        public ActionResult ViewDodajManifestaciju()
        {

            return View("ProdavacDManifestacijuView");
        }

       [HttpPost]
        public ActionResult DodajManifestaciju()
        {
            int izmenaIlidodaj = Int32.Parse(Request["IzmeniIliDodaj"]);
            string naziv = Request["Naziv"];
            string tipmanifestacije = Request["TipManifestacije"];
            int brojMesta;
            int godina;
            int mesec;
            int dan;
            int sat;
            int minut;
            int cena;
            double geografskaduzina;
            double geografskasirina;
            string grad = Request["Grad"].Trim(' ');
            string ulica = Request["Ulica"].Trim(' ');
            string brojulice = Request["BrojUlice"].Trim(' ');
            int postanskibroj;
            if (Int32.TryParse(Request["PostanskiBroj"], out postanskibroj) == false ||  Double.TryParse(Request["GeografskaDuzina"], out geografskaduzina) == false ||  Double.TryParse(Request["GeografskaSirina"],out geografskasirina) == false || Int32.TryParse(Request["BrojMesta"], out brojMesta) == false || Int32.TryParse(Request["GodinaManifestacije"], out godina) == false || Int32.TryParse(Request["MesecManifestacije"], out mesec) == false
                || Int32.TryParse(Request["DanManifestacije"], out dan) == false || Int32.TryParse(Request["SatManifestacije"], out sat) == false || Int32.TryParse(Request["MinutManifestacije"], out minut) == false || Int32.TryParse(Request["Cena"], out cena) == false)
            {
                TempData["Alert"] = "Pogresno uneti podaci";
                return View("ProdavacDManifestacijuView");
            }
            
            string slika = Request["Slika"];
            if ( naziv == "" || naziv.Any(char.IsDigit)== true || grad == "" || grad.Any(char.IsDigit) == true || ulica == "" || ulica.Any(char.IsDigit) == true || brojulice == "")
            {
                TempData["Alert"] = "Pogresno uneti podaci";
                return View("ProdavacDManifestacijuView");

            }
            if (izmenaIlidodaj == 1 && slika == "")
            {
                TempData["Alert"] = "Pogresno uneti podaci";
                return View("ProdavacDManifestacijuView");
            }
            else if (slika == "")
            {
                slika = Request["StaraSlika"];
            }
            DateTime datum;
            manifestacije = new Manifestacije();
            try
            {
                datum = new DateTime(godina, mesec, dan, sat, minut, 0);
                DateTime trenutni = DateTime.Now;
                if (  (datum - trenutni).TotalDays <=1)
                {
                    TempData["Alert"] = "Datum mora biti dva dana unapred";
                    return View("ProdavacDManifestacijuView");
                }
            }
            catch
            {
                TempData["Alert"] = "Pogresno unet datum";
                return View("ProdavacDManifestacijuView");
            }
            Korisnik ulogovan = (Korisnik)Session["Ulogovan"];
            Manifestacija manifestacija = new Manifestacija(manifestacije.lista_manifestacija.Count + 1, ulogovan.Id, naziv, (TipManifestacije)Int32.Parse(tipmanifestacije), brojMesta, datum, cena, Status.Neaktivan, slika, new Lokacija(geografskaduzina, geografskasirina, grad, ulica, brojulice, postanskibroj));
            if (izmenaIlidodaj == 1 && manifestacije.DodajManifestaciju(manifestacija) == false)
            {
                TempData["Alert"] = "Vec postoji manifestacija na toj lokaciji u to vreme.";
                return View("ProdavacDManifestacijuView");
            }
            else if (izmenaIlidodaj == 0)
            {
                manifestacija.Id = Int32.Parse(Request["Id"]);
                if (manifestacije.IzmeniManifestaciju(manifestacija) == false)
                {
                    TempData["Alert"] = "Vec postoji manifestacija na toj lokaciji u to vreme.";
                    return View("ProdavacIzmeniManifestacijuView");
                }
            }

            return Index();
        }

        [HttpPost]
        public ActionResult Registracija()
        {
            int godina;
            int mesec;
            int dan;
            if (Int32.TryParse(Request["GodinaRodjenja"],out godina) == false || Int32.TryParse(Request["MesecRodjenja"], out mesec)== false || Int32.TryParse(Request["DanRodjenja"], out dan) == false)
            {
                TempData["Alert"] = "Pogresno uneti podaci";
                return View("RegistrujKorisnikaView");
            }
         
            if (Request["KorisnickoIme"].Trim(' ') == "" || Request["Lozinka"].Trim(' ') == "" || Request["Ime"].Trim(' ') == ""|| Request["Ime"].Trim(' ').Any(char.IsDigit) || Request["Prezime"].Trim(' ') == "" || Request["Prezime"].Trim(' ').Any(char.IsDigit) || (mesec <1 || mesec>12) || (dan < 1 || dan > 31) || godina >= DateTime.Now.Year)
            {
                TempData["Alert"] = "Pogresno uneti podaci";
                return View("RegistrujKorisnikaView");
            }
            users = new Users();
            Korisnik k = new Korisnik();
            k.Id = users.lista_korisnika.Count + 1;
            if (Session["Ulogovan"]!=null)
            {
                k.Uloga = Uloga.Prodavac;
            }
            k.KorisnickoIme = Request["KorisnickoIme"].Trim(' '); 
            k.Lozinka = Request["Lozinka"].Trim(' ');
            k.Ime = Request["Ime"].Trim(' ');
            k.Prezime = Request["Prezime"].Trim(' ');
            string s = Request["Pol"];

            if (Request["Pol"] == "Muski")
            {
                k.Pol = Pol.MUSKI;
            }
            else
            {
                k.Pol = Pol.ZENSKI;
            }
            k.DatumRodjenja = Request["GodinaRodjenja"] + "/" + Request["MesecRodjenja"] + "/" + Request["DanRodjenja"];
            if (users.DodajKorisnike(k))
            {
                if (Session["Ulogovan"]!= null)
                {
                    return PrikazKorisnika();
                }
                Random rand = new Random();

                karte.DodajKarte(new Karta(karte.lista_karata.Count + 1, "NekaManifestacija" + rand.Next(0, 20), new DateTime(rand.Next(1997, 2010), rand.Next(1, 12), rand.Next(1, 28)), rand.Next(1, 500), k, (TipKarte)rand.Next(1, 3)));
                karte.DodajKarte(new Karta(karte.lista_karata.Count + 1, "NekaManifestacija" + rand.Next(0, 20), new DateTime(rand.Next(1997, 2010), rand.Next(1, 12), rand.Next(1, 28)), rand.Next(1, 500), k, (TipKarte)rand.Next(1, 3)));
                karte.DodajKarte(new Karta(karte.lista_karata.Count + 1, "NekaManifestacija" + rand.Next(0, 20), new DateTime(rand.Next(1997, 2010), rand.Next(1, 12), rand.Next(1, 28)), rand.Next(1, 500), k, (TipKarte)rand.Next(1, 3)));
                karte.DodajKarte(new Karta(karte.lista_karata.Count + 1, "NekaManifestacija" + rand.Next(0, 20), new DateTime(rand.Next(1997, 2010), rand.Next(1, 12), rand.Next(1, 28)), rand.Next(1, 500), k, (TipKarte)rand.Next(1, 3)));
                karte.DodajKarte(new Karta(karte.lista_karata.Count + 1, "NekaManifestacija" + rand.Next(0, 20), new DateTime(rand.Next(1997, 2010), rand.Next(1, 12), rand.Next(1, 28)), rand.Next(1, 500), k, (TipKarte)rand.Next(1, 3)));


                Session["Ulogovan"] = k;
                return View("KupacHomepageView");
            }
            else
            {
                TempData["Alert"] = "Vec postoji korisnik sa tim korisnickim imenom";
                return View("RegistrujKorisnikaView");
            }
        }
        [HttpPost]
        public ActionResult Uloguj()
        {
            Korisnik k = new Korisnik();
            k.KorisnickoIme = Request["KorisnickoIme"];
            k.Lozinka = Request["Lozinka"];
            users = new Users();
            k = users.ProveriLog(k);
            if (k == null)
            {
                TempData["Alert"] = "Ta kombinacija korisnickog imena i lozinke ne postoji";
                return View("UlogujSeView");
            }
            else
            {
                Session["Ulogovan"] = k;
                return Index();
            }
        }
        public ActionResult PrikaziManifestaciju()
        {
            manifestacije = new Manifestacije();
            komentari = new Komentari();
            Session["Manifestacija"] = manifestacije.Pronadji(Int32.Parse(Request["Id"]));
            Session["AktivniKomentari"] = komentari.VratiAktivne(Int32.Parse(Request["Id"]));
            Session["NeaktivniKomentari"] = komentari.VratiNeaktivne(Int32.Parse(Request["Id"]));
            return View("PrikaziManifestacijuView");
        }

        [HttpPost]
        public ActionResult AktivirajKomentar()
        {
            komentari = new Komentari();
            komentari.AktivirajKomentar(Int32.Parse(Request["Id"]));
            Session["AktivniKomentari"] = komentari.VratiAktivne( ((Manifestacija)Session["Manifestacija"]).Id);
            Session["NeaktivniKomentari"] = komentari.VratiNeaktivne(((Manifestacija)Session["Manifestacija"]).Id);

            return View("PrikaziManifestacijuView");
        }


        [HttpPost]
        public ActionResult DeaktivirajKomentar()
        {
            komentari = new Komentari();
            komentari.DeaktivirajKomentar(Int32.Parse(Request["Id"]));
            Session["AktivniKomentari"] = komentari.VratiAktivne(((Manifestacija)Session["Manifestacija"]).Id);
            Session["NeaktivniKomentari"] = komentari.VratiNeaktivne(((Manifestacija)Session["Manifestacija"]).Id);

            return View("PrikaziManifestacijuView");
        }
        [HttpPost]
        public ActionResult DodajKomentar()
        {
            komentari = new Komentari();

            Komentar komentar = new Komentar(komentari.lista_komentara.Count + 1, ((Korisnik)Session["Ulogovan"]).KorisnickoIme, ((Manifestacija)Session["Manifestacija"]).Id, Request["Tekst"], Int32.Parse(Request["Ocena"]), false);
            komentari.DodajKomentar(komentar);
            return View("PrikaziManifestacijuView");
        }


        [HttpPost]
        public ActionResult AktivirajManifestaciju()
        {
            manifestacije = new Manifestacije();
            manifestacije.AktivirajManifestaciju(Int32.Parse(Request["Id"]));
            Session["Manifestacije"] = Manifestacije.filter_lista;

            return View("AdminHomepageView");
        }
        [HttpPost]
        public ActionResult DeaktivirajManifestaciju()
        {
            manifestacije = new Manifestacije();
            manifestacije.DeaktivirajManifestaciju(Int32.Parse(Request["Id"]));
            Session["Manifestacije"] = Manifestacije.filter_lista;

            return View("AdminHomepageView");
        }
        [HttpPost]
        public ActionResult ObrisiManifestaciju()
        {
            manifestacije = new Manifestacije();
            manifestacije.ObrisiManifestaciju(Int32.Parse(Request["Id"]));
            Session["Manifestacije"] = Manifestacije.filter_lista;

            return View("AdminHomepageView");
        }
        [HttpPost]
        public ActionResult IzmeniManifestacijuView()
        {
            manifestacije = new Manifestacije();
            Session["Manifestacija"]  = manifestacije.VratiManifestaicju(Int32.Parse(Request["Id"]));

            return View("ProdavacIzmeniManifestacijuView");
        }


        public ActionResult SortirajManifestacije()
        {
            int tipSortiranja = Int32.Parse(Request["TipSortiranja"]);
            int rast = Int32.Parse(Request["Rast"]);

            if (tipSortiranja == 0)
            {// naziv
                if (rast == 0)
                {
                    for (int i = 0; i < Manifestacije.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Manifestacije.filter_lista.Count; j++)
                        {
                            if (Manifestacije.filter_lista[i].Naziv.CompareTo(Manifestacije.filter_lista[j].Naziv) > 0)
                            {
                                Manifestacija temp = Manifestacije.filter_lista[i];
                                Manifestacije.filter_lista[i] = Manifestacije.filter_lista[j];
                                Manifestacije.filter_lista[j] = temp;
                            }
                        }
                    }
                }
                else if (rast == 1)
                {
                    for (int i = 0; i < Manifestacije.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Manifestacije.filter_lista.Count; j++)
                        {
                            if (Manifestacije.filter_lista[i].Naziv.CompareTo(Manifestacije.filter_lista[j].Naziv) < 0)
                            {
                                Manifestacija temp = Manifestacije.filter_lista[i];
                                Manifestacije.filter_lista[i] = Manifestacije.filter_lista[j];
                                Manifestacije.filter_lista[j] = temp;
                            }
                        }
                    }
                }
            }
            else if (tipSortiranja == 1)
            {// datum
                if (rast == 0)
                {
                    for (int i = 0; i < Manifestacije.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Manifestacije.filter_lista.Count; j++)
                        {
                            if (Manifestacije.filter_lista[i].Datum > Manifestacije.filter_lista[j].Datum)
                            {
                                Manifestacija temp = Manifestacije.filter_lista[i];
                                Manifestacije.filter_lista[i] = Manifestacije.filter_lista[j];
                                Manifestacije.filter_lista[j] = temp;
                            }
                        }
                    }
                }
                else if (rast == 1)
                {
                    for (int i = 0; i < Manifestacije.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Manifestacije.filter_lista.Count; j++)
                        {
                            if (Manifestacije.filter_lista[i].Datum < Manifestacije.filter_lista[j].Datum)
                            {
                                Manifestacija temp = Manifestacije.filter_lista[i];
                                Manifestacije.filter_lista[i] = Manifestacije.filter_lista[j];
                                Manifestacije.filter_lista[j] = temp;
                            }
                        }
                    }
                }
            }
            else if (tipSortiranja == 2)
            {//cena
                if (rast == 0)
                {
                    for (int i = 0; i < Manifestacije.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Manifestacije.filter_lista.Count; j++)
                        {
                            if (Manifestacije.filter_lista[i].CenaKarte > Manifestacije.filter_lista[j].CenaKarte)
                            {
                                Manifestacija temp = Manifestacije.filter_lista[i];
                                Manifestacije.filter_lista[i] = Manifestacije.filter_lista[j];
                                Manifestacije.filter_lista[j] = temp;
                            }
                        }
                    }
                }
                else if (rast == 1)
                {
                    for (int i = 0; i < Manifestacije.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Manifestacije.filter_lista.Count; j++)
                        {
                            if (Manifestacije.filter_lista[i].CenaKarte < Manifestacije.filter_lista[j].CenaKarte)
                            {
                                Manifestacija temp = Manifestacije.filter_lista[i];
                                Manifestacije.filter_lista[i] = Manifestacije.filter_lista[j];
                                Manifestacije.filter_lista[j] = temp;
                            }
                        }
                    }
                }
            }
            else if (tipSortiranja == 3)
            {//grad
                if (rast == 0)
                {
                    for (int i = 0; i < Manifestacije.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Manifestacije.filter_lista.Count; j++)
                        {
                            if (Manifestacije.filter_lista[i].Lokacija.Grad.CompareTo(Manifestacije.filter_lista[j].Lokacija.Grad) > 0)
                            {
                                Manifestacija temp = Manifestacije.filter_lista[i];
                                Manifestacije.filter_lista[i] = Manifestacije.filter_lista[j];
                                Manifestacije.filter_lista[j] = temp;
                            }
                        }
                    }
                }
                else if (rast == 1)
                {
                    for (int i = 0; i < Manifestacije.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Manifestacije.filter_lista.Count; j++)
                        {
                            if (Manifestacije.filter_lista[i].Lokacija.Grad.CompareTo(Manifestacije.filter_lista[j].Lokacija.Grad) < 0)
                            {
                                Manifestacija temp = Manifestacije.filter_lista[i];
                                Manifestacije.filter_lista[i] = Manifestacije.filter_lista[j];
                                Manifestacije.filter_lista[j] = temp;
                            }
                        }
                    }
                }
            }
            Korisnik ulogovani = (Korisnik)Session["Ulogovan"];

            Session["Manifestacije"] = Manifestacije.filter_lista;
            if (ulogovani == null)
            {
                return View("Index");
            }
            else if (ulogovani.Uloga == Uloga.Kupac)
            {
                return View("KupacHomepageView");
            }
            else if (ulogovani.Uloga == Uloga.Prodavac)
            {
                return View("ProdavacHomepageView");
            }
            else if (ulogovani.Uloga == Uloga.Administrator)
            {
                return View("AdminHomepageView");
            }
            else
            {
                return View();
            }
        }

        public ActionResult FilterManifestacija()
        {
            manifestacije = new Manifestacije();
            string naziv = Request["Naziv"];
            string grad = Request["Grad"];
            string ulica = Request["Ulica"];
            int tipManifestacije = Int32.Parse(Request["TipManifestacije"]);
            int cenaod = -1;
            int cenado = -1;
            Korisnik ulogovani = (Korisnik)Session["Ulogovan"];

            if (Int32.TryParse(Request["CenaDo"], out cenado) == false || Int32.TryParse(Request["CenaOd"], out cenaod) == false)
            {
                if (ulogovani == null)
                {
                    return View("Index");
                }
                else if (ulogovani.Uloga == Uloga.Kupac)
                {
                    return View("KupacKarteView");
                }
                else
                {
                    return View("AdminKarteView");
                }
            }
            bool datum = false;



            DateTime datumod = new DateTime();
            DateTime datumdo = new DateTime();
            if (Request["GodinaOd"]!= "" && Request["MesecOd"]!="" && Request["DanOd"]!="" && Request["GodinaDo"]!="" && Request["MesecDo"] != "" && Request["DanDo"] != "")
            {
                try
                {
                    int godinaod = Int32.Parse(Request["GodinaOd"]);
                    int mesecod = Int32.Parse(Request["MesecOd"]);
                    int danod = Int32.Parse(Request["DanOd"]);
                    int godinado = Int32.Parse(Request["GodinaDo"]);
                    int mesecdo = Int32.Parse(Request["MesecDo"]);
                    int dando = Int32.Parse(Request["DanDo"]);
                    datumod = new DateTime(godinaod, mesecod, danod, 0, 0, 0);
                    datumdo = new DateTime(godinado, mesecdo, dando, 0, 0, 0);
                    datum = true;
                }
                catch
                {
                    datum = false;
                }
            }

            if (ulogovani == null)
            {
                Manifestacije.filter_lista = manifestacije.VratiAktivne();
            }
            else if (ulogovani.Uloga == Uloga.Kupac)
            {
                Manifestacije.filter_lista = manifestacije.VratiAktivne();
            }
            else if (ulogovani.Uloga == Uloga.Prodavac)
            {
                Manifestacije.filter_lista = manifestacije.VratiValidneSaId(ulogovani.Id);
            }
            else if (ulogovani.Uloga == Uloga.Administrator)
            {
                Manifestacije.filter_lista=  manifestacije.VratiValidne();
            }


            foreach (var item in Manifestacije.filter_lista.ToList())
            {
                if (naziv != "" && item.Naziv.StartsWith(naziv) == false)
                {
                    Manifestacije.filter_lista.Remove(item);
                }
                if (grad != "" && item.Lokacija.Grad.StartsWith(grad) == false)
                {
                    Manifestacije.filter_lista.Remove(item);
                }
                if (ulica != "" && item.Lokacija.Ulica.StartsWith(ulica) == false)
                {
                    Manifestacije.filter_lista.Remove(item);
                }
                if (tipManifestacije != -1 && item.TipManifestacije != (TipManifestacije)tipManifestacije)
                {
                    Manifestacije.filter_lista.Remove(item);
                }
                if ( (cenaod != -1 || cenado!= -1)  && (item.CenaKarte < cenaod || item.CenaKarte> cenado))
                {
                    Manifestacije.filter_lista.Remove(item);
                }
                if (datum == true && (item.Datum < datumod || item.Datum> datumdo))
                {
                    Manifestacije.filter_lista.Remove(item);
                }
            }
            Session["Manifestacije"] = Manifestacije.filter_lista;
            if (ulogovani == null)
            {
                return View("Index");
            }
            else if (ulogovani.Uloga == Uloga.Kupac)
            {
                return View("KupacHomepageView");
            }
            else if (ulogovani.Uloga == Uloga.Prodavac)
            {
                return View("ProdavacHomepageView");
            }
            else if (ulogovani.Uloga == Uloga.Administrator)
            {
                return View("AdminHomepageView");
            }
            else
            {
                return View();
            }
        }

        public ActionResult SortirajKarte()
        {
            int tipSortiranja = Int32.Parse(Request["TipSortiranja"]);
            int rast = Int32.Parse(Request["Rast"]);

            if (tipSortiranja == 0)
            {// naziv
                if (rast == 0)
                {
                    for (int i = 0; i < Karte.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Karte.filter_lista.Count; j++)
                        {
                            if (Karte.filter_lista[i].NazivManifestacije.CompareTo(Karte.filter_lista[j].NazivManifestacije) > 0)
                            {
                                Karta temp = Karte.filter_lista[i];
                                Karte.filter_lista[i] = Karte.filter_lista[j];
                                Karte.filter_lista[j] = temp;
                            }
                        }
                    }
                }

                else if (rast == 1)
                {
                    for (int i = 0; i < Karte.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Karte.filter_lista.Count; j++)
                        {
                            if (Karte.filter_lista[i].NazivManifestacije.CompareTo(Karte.filter_lista[j].NazivManifestacije) < 0)
                            {
                                Karta temp = Karte.filter_lista[i];
                                Karte.filter_lista[i] = Karte.filter_lista[j];
                                Karte.filter_lista[j] = temp;
                            }

                        }
                    }
                }
            }
            else if (tipSortiranja == 1)
            {// datum
                if (rast == 0)
                {
                    for (int i = 0; i < Karte.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Karte.filter_lista.Count; j++)
                        {
                            if (Karte.filter_lista[i].Datum > Karte.filter_lista[j].Datum)
                            {
                                Karta temp = Karte.filter_lista[i];
                                Karte.filter_lista[i] = Karte.filter_lista[j];
                                Karte.filter_lista[j] = temp;
                            }
                        }
                    }
                }

                else if (rast == 1)
                {
                    for (int i = 0; i < Karte.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Karte.filter_lista.Count; j++)
                        {
                            if (Karte.filter_lista[i].Datum < Karte.filter_lista[j].Datum)
                            {
                                Karta temp = Karte.filter_lista[i];
                                Karte.filter_lista[i] = Karte.filter_lista[j];
                                Karte.filter_lista[j] = temp;
                            }
                        }
                    }
                }
            }
            else if (tipSortiranja == 2)
            {//cena
                if (rast == 0)
                {
                    for (int i = 0; i < Karte.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Karte.filter_lista.Count; j++)
                        {
                            if (Karte.filter_lista[i].Cena > Karte.filter_lista[j].Cena)
                            {
                                Karta temp = Karte.filter_lista[i];
                                Karte.filter_lista[i] = Karte.filter_lista[j];
                                Karte.filter_lista[j] = temp;
                            }
                        }
                    }
                }
                else if (rast == 1)
                {
                    for (int i = 0; i < Karte.filter_lista.Count - 1; i++)
                    {
                        for (int j = i + 1; j < Karte.filter_lista.Count; j++)
                        {
                            if (Karte.filter_lista[i].Cena < Karte.filter_lista[j].Cena)
                            {
                                Karta temp = Karte.filter_lista[i];
                                Karte.filter_lista[i] = Karte.filter_lista[j];
                                Karte.filter_lista[j] = temp;
                            }
                        }
                    }
                }
            }

            Session["Karte"] = Karte.filter_lista;
            Korisnik ulogovani = (Korisnik)Session["Ulogovan"];

            if (ulogovani.Uloga == Uloga.Kupac)
            {
                return View("KupacKarteView");
            }
            else
            {
                return View("AdminKarteView");
            }
        }
        public ActionResult FilterKarte()
        {
            manifestacije = new Manifestacije();
            string naziv = Request["Naziv"];
            int tipkarte = Int32.Parse(Request["TipKarte"]);
            int cenaod = -1;
            int cenado = -1;
            Korisnik ulogovani = (Korisnik)Session["Ulogovan"];

            if (Int32.TryParse(Request["CenaDo"],out cenado) == false || Int32.TryParse(Request["CenaOd"], out cenaod) == false)
            {
                if (ulogovani.Uloga == Uloga.Kupac)
                {
                    return View("KupacKarteView");
                }
                else
                {
                    return View("AdminKarteView");
                }
            }
            bool datum = false;

            DateTime datumod = new DateTime();
            DateTime datumdo = new DateTime();
            if (Request["GodinaOd"] != "" && Request["MesecOd"] != "" && Request["DanOd"] != "" && Request["GodinaDo"] != "" && Request["MesecDo"] != "" && Request["DanDo"] != "")
            {
                try
                {
                    int godinaod = Int32.Parse(Request["GodinaOd"]);
                    int mesecod = Int32.Parse(Request["MesecOd"]);
                    int danod = Int32.Parse(Request["DanOd"]);
                    int godinado = Int32.Parse(Request["GodinaDo"]);
                    int mesecdo = Int32.Parse(Request["MesecDo"]);
                    int dando = Int32.Parse(Request["DanDo"]);
                    datumod = new DateTime(godinaod, mesecod, danod, 0, 0, 0);
                    datumdo = new DateTime(godinado, mesecdo, dando, 0, 0, 0);
                    datum = true;
                }
                catch
                {
                    datum = false;
                }
            }

            if (ulogovani.Uloga == Uloga.Kupac)
            {
                Karte.filter_lista = karte.VratiAktivne(ulogovani.Id);
            }
            else
            {
                Karte.filter_lista = karte.VratiSve();
            }
            foreach (var item in Karte.filter_lista.ToList())
            {
                if (naziv != "" && item.NazivManifestacije.StartsWith(naziv) == false)
                {
                    Karte.filter_lista.Remove(item);
                }
                if (tipkarte != -1 && item.TipKarte != (TipKarte)tipkarte)
                {
                    Karte.filter_lista.Remove(item);
                }
                if ((cenaod != -1 || cenado != -1)  && (item.Cena < cenaod || item.Cena > cenado))
                {
                    Karte.filter_lista.Remove(item);
                }
                if (datum == true && (item.Datum < datumod || item.Datum > datumdo))
                {
                    Karte.filter_lista.Remove(item);
                }
            }

            
            Session["Karte"] = Karte.filter_lista;
            
            if (ulogovani.Uloga == Uloga.Kupac)
            {
                return View("KupacKarteView");
            }
            else
            {
                return View("AdminKarteView");
            }
        }
        public ActionResult PrikazKarti()
        {
            karte = new Karte();


            Korisnik ulogovani = (Korisnik)Session["Ulogovan"];
            if (ulogovani.Uloga == Uloga.Kupac)
            {
                Session["Karte"] = karte.VratiAktivne(ulogovani.Id);
                return View("KupacKarteView");
            }
            else
            {
                Session["Karte"] = karte.VratiSve();
                return View("AdminKarteView");
            }
        }
        [HttpPost]
        public ActionResult ObrisiKartu()
        {
            karte = new Karte();
            karte.ObrisiKartu(Int32.Parse(Request["Id"]));
            
            return View("AdminKarteView");
            
        }
    }
}